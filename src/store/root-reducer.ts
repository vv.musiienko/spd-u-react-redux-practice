import {combineReducers} from "@reduxjs/toolkit";
import {stations} from "../modules/stations/services/reducers";
import {countries} from "../modules/country-select/services/reducers";

export const rootReducer = combineReducers({
    stations,
    countries
})