import React, {useEffect} from "react";
import {Col, Row} from "react-bootstrap";
import {StationCard} from "./components/station-card";
import {useAppDispatch, useAppSelector} from "../../store/hooks";
import {loadStations} from "./services/actions";
import {getStations} from "./services/selectors";

interface Props  {
    country: string
}

export const AllStations = ({country}: Props) => {
    const dispatch = useAppDispatch();
    const stations = useAppSelector(state => getStations(state).all);

    useEffect(() => {
        dispatch(loadStations(country));
    }, [country]);

    return (
        <Row xs={1} md={2} className="g-4">
            {stations.map(station => (
                <Col key={station.stationuuid}>
                    <StationCard station={station}/>
                </Col>
            ))}
        </Row>
    )
}