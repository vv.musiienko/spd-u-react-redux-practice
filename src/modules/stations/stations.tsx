import {Tab, Tabs} from "react-bootstrap";
import React from "react";
import {AllStations} from "./all-stations";
import {FavouriteStations} from "./favourite-stations";

interface Props {
    country: string
}

export const Stations = ({country}: Props) => {

    return (
        <Tabs defaultActiveKey="all-stations" className="mb-3">
            <Tab eventKey="all-stations" title="Stations">
                <AllStations country={country}/>
            </Tab>
            <Tab eventKey="favourites" title="Favourites">
                <FavouriteStations/>
            </Tab>
        </Tabs>
    )
}