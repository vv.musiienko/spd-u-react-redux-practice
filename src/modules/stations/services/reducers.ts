import {Station} from "../../../typedef";
import {createReducer} from "@reduxjs/toolkit";
import {loadStations, setFavourite} from "./actions";

type State = {
    all: Station[],
    favourites: {
        [id: string]: boolean
    }
}

const defaultState = {
    all: [],
    favourites: {}
} as State;


export const stations = createReducer(defaultState, builder =>
    builder
        .addCase(loadStations.fulfilled, (state, action) => {
            state.all = action.payload;
        })
        .addCase(setFavourite, (state, action) => {
            state.favourites[action.payload.id] = action.payload.favourite;
        })
)