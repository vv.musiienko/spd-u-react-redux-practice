import {createAction, createAsyncThunk} from "@reduxjs/toolkit";

export const loadCountries = createAsyncThunk('countries/load', async () => {
    return fetch('http://all.api.radio-browser.info/json/countries')
        .then(res => res.json())
});

export const setActiveCountry = createAction<{country: string}>('countries/set-active');