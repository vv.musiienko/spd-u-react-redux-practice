import {Country} from "../../../typedef";
import {createReducer} from "@reduxjs/toolkit";
import {loadCountries, setActiveCountry} from "./actions";

type State = {
    all: Country[],
    selectedCountry: string | null
}

const defaultState = {
    all: [],
    selectedCountry: null
} as State;


export const countries = createReducer(defaultState, builder =>
    builder
        .addCase(loadCountries.fulfilled, (state, action) => {
            state.all = action.payload;
        })
        .addCase(setActiveCountry, (state, action) => {
            state.selectedCountry = action.payload.country;
        })
)