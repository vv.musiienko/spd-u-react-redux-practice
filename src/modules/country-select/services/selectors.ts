import {AppState} from "../../../store/store";

export const getCountries = (state: AppState) => {
    return [...state.countries.all]
        .sort((a, b) => b.stationcount - a.stationcount);
}