import { Form } from "react-bootstrap";
import {ChangeEvent, useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../store/hooks";
import {loadCountries} from "./services/actions";
import {getCountries} from "./services/selectors";


interface Props {
    onSelect: (country: string) => void
}

export const CountrySelect = ({onSelect}: Props) => {
    const dispatch = useAppDispatch();
    const countries = useAppSelector(getCountries);

    useEffect(() => {
        dispatch(loadCountries());
    }, []);

    const onChange = (e: ChangeEvent<HTMLSelectElement>) => {
        onSelect(e.target.value);
    }

    return <>
        <Form.Group className="mb-3">
            <Form.Label>Select country</Form.Label>
            <Form.Select onChange={onChange}>
                {countries.map(country => <option key={country.name} value={country.name}>{country.name} ({country.stationcount})</option>)}
            </Form.Select>
        </Form.Group>
    </>;
}