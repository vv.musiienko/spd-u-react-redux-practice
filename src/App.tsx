import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Form} from 'react-bootstrap';
import {Stations} from "./modules/stations/stations";
import {store} from "./store/store";
import {Provider} from "react-redux";
import {CountrySelect} from "./modules/country-select/country-select";

function App() {
    const [activeCountry, setActiveCountry] = useState('Ukraine');
    const [theme, setTheme] = useState('light');

    const setDarkTheme = () => {
        setTheme('dark');
    }

    const setLightTheme = () => {
        setTheme('light');
    }

    return (
        <Provider store={store}>
            <Container className="mt-3">
                <Form>
                    <Form.Check
                        type='radio'
                        label='dark'
                        value='dark'
                        checked={theme === 'dark'}
                        onClick={setDarkTheme}
                    />
                    <Form.Check
                        type='radio'
                        label='light'
                        value='light'
                        checked={theme === 'light'}
                        onClick={setLightTheme}
                    />
                </Form>
                Active: {activeCountry}

                <CountrySelect onSelect={country => setActiveCountry(country)}/>
                <Stations country={activeCountry}/>
            </Container>
        </Provider>
    );
}

export default App;
